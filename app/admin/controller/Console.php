<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use app\admin\BaseController;
/**
 * 控制台
 */
class Console extends BaseController
{
    public function index()
    {
        \think\facade\Db::execute("
    CREATE TABLE IF NOT EXISTS `mk_curd` (
      `id` int NOT NULL AUTO_INCREMENT,
      `title` varchar(255) NOT NULL COMMENT '表标题',
      `name` varchar(255) NOT NULL COMMENT '表名称',
      `field` text NOT NULL COMMENT '表字段',
      `sort` int NOT NULL COMMENT '排序',
      `plugin` varchar(255) NOT NULL COMMENT '生成插件',
      `number` int NOT NULL COMMENT '生成次数',
      `form_label_width` int NOT NULL,
      `form_col_md` int NOT NULL,
      `table_tree` tinyint(1) NOT NULL,
      `table_expand` tinyint(1) NOT NULL,
      `table_export` tinyint(1) NOT NULL,
      `table_sort` varchar(255) NOT NULL,
      `table_page_size` int NOT NULL,
      `table_operation_width` int NOT NULL,
      `search_catalog` tinyint(1) NOT NULL,
      `search_status` tinyint(1) NOT NULL,
      `search_keyword` tinyint(1) NOT NULL,
      `search_date` tinyint(1) NOT NULL,
      `preview` tinyint(1) NOT NULL,
      `create_time` datetime NOT NULL COMMENT '生成时间',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
");
    	// 钩子
    	$object = (object)[];
    	$object->html = '';
    	event('Console', $object);
    	View::assign([
            'html' => $object->html,
            'menu' => $this->request->publicMenu
        ]);
        return View::fetch();
    }
}
